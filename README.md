MyPlaces is already available on Google Play Store! [(click!)](https://play.google.com/store/apps/details?id=com.marcinbaranski.mymovies)

The app was developed as second quest to take part in 5-days Android Bootcamp.
It is a simple application to see lists of top rated, most popular or upcoming movies. 
In addition, you can use build-in movies search to find the movie you are looking for.
But that is not all... You can rate movies and then see list of all rated movies by you!

To developed it, I used kotlin, Retrofit to connect with API, a little of RxJava, GSON and MVP pattern.
