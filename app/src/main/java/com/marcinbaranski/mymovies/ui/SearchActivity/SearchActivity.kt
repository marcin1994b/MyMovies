package com.marcinbaranski.mymovies.ui.SearchActivity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.jakewharton.rxbinding2.widget.textChanges
import com.marcinbaranski.mymovies.R
import com.marcinbaranski.mymovies.ui.SearchActivity.SearchFragment.SearchFragment
import kotlinx.android.synthetic.main.activity_search.*

class SearchActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        initToolbar()
        initFragment()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.serach_activity_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.search -> {
                (supportFragmentManager.fragments.get(0) as SearchFragment)
                        .downloadData(search_edittext.text.toString())
            }
        }
        return false
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun initToolbar(){
        setSupportActionBar(my_toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.let{
            it.setDisplayHomeAsUpEnabled(true);
            it.setDisplayShowHomeEnabled(true);
        }

    }

    private fun initFragment(){
        val fragment = SearchFragment()
        supportFragmentManager.beginTransaction()
                .add(R.id.container, fragment)
                .commit()
        fragment.setObservableOnEditText(search_edittext.textChanges())
    }

}
