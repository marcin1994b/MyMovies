package com.marcinbaranski.mymovies.ui.MainActivity

import com.marcinbaranski.mymovies.ui.PresenterContracts

class MainActivityPresenter(val view: PresenterContracts.PublishToMainActivity):
        PresenterContracts.MainActivityPresenter {

    override fun onSearchMenuButtonClick() {
        view.startSearchActivity()
    }

    override fun onRatedMoviesMenuButtonClick() {
        view.startRatedMoviesActivity()
    }
}