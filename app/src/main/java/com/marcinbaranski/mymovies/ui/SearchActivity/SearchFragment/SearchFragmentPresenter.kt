package com.marcinbaranski.mymovies.ui.SearchActivity.SearchFragment

import com.marcinbaranski.mymovies.Model.APIHandler
import com.marcinbaranski.mymovies.Model.Providers.NetworkProvider
import com.marcinbaranski.mymovies.ui.MyFragmentsPresenter
import com.marcinbaranski.mymovies.ui.PresenterContracts

class SearchFragmentPresenter(view: PresenterContracts.PublishToFragmentView,
                              val query: String,
                              networkProvider: NetworkProvider) :
        PresenterContracts.FragmentsPresenter, MyFragmentsPresenter(view, networkProvider) {

    override fun downloadData() {
        val apiHandler = APIHandler()
        getDataFromObservable(apiHandler.getSearchMovies(query, downloadedPages))
    }
}