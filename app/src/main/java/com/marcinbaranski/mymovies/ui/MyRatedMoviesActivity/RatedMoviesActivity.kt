package com.marcinbaranski.mymovies.ui.MyRatedMoviesActivity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.marcinbaranski.mymovies.R
import com.marcinbaranski.mymovies.ui.MyRatedMoviesActivity.RatedMoviesFragment.RatedMoviesFragment
import kotlinx.android.synthetic.main.activity_rated_movies.*


class RatedMoviesActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rated_movies)
        initToolbar()
        initFragment()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun initToolbar(){
        my_toolbar.title = getString(R.string.title_rated_movie_activity)
        setSupportActionBar(my_toolbar)
        supportActionBar?.let{
            it.setDisplayHomeAsUpEnabled(true);
            it.setDisplayShowHomeEnabled(true);
        }

    }

    private fun initFragment(){
        supportFragmentManager.beginTransaction()
                .add(R.id.container, RatedMoviesFragment())
                .commit()
    }
}