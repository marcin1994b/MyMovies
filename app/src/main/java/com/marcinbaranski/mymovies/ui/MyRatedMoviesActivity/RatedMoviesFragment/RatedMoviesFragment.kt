package com.marcinbaranski.mymovies.ui.MyRatedMoviesActivity.RatedMoviesFragment

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.marcinbaranski.mymovies.Model.MyStrings
import com.marcinbaranski.mymovies.Model.Providers.NetworkProvider
import com.marcinbaranski.mymovies.R
import com.marcinbaranski.mymovies.ui.MyFragment

class RatedMoviesFragment : MyFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        mainView = inflater?.inflate(R.layout.fragment_layout, container, false)
        initListView(mainView)
        downloadData()
        return mainView
    }

    private fun downloadData(){
        val guestID = getGuestID()
        if(!guestID.isEmpty()) {
            initPresenter(guestID)
            presenter.downloadMoviesPage()
        }
    }

    private fun initPresenter(guestId: String){
        presenter = RatedMoviesFragmentPresenter(this, guestId, NetworkProvider(context))
    }

    private fun getGuestID() : String{
        val preferences = context.getSharedPreferences(MyStrings.PREFERENCES_NAME, Activity.MODE_PRIVATE)
        preferences?.let {
            return it.getString(MyStrings.PREFERENCES_GUEST_ID, "")
        }
        return ""
    }
}