package com.marcinbaranski.mymovies.ui

import com.marcinbaranski.mymovies.Model.MovieResponse.MovieGeneral

interface PresenterContracts {

    interface PublishToView {

        fun showToast(message: String)
        //fun isNetworkAvailable() : Boolean
        fun showInformationView(mainMessage: String, secondMessage: String)
        fun hideInformationView()
    }


    interface PublishToFragmentView : PublishToView {

        fun startDetailsActivity(movieId: Int)
        fun addElementsToViewList(list: ArrayList<MovieGeneral>)
        fun showOnlyFooterButtonView()
        fun showOnlyInfoFooterView(mainMessage: String, secondMessage: String)
        fun hideAllFooters()
        fun hideListView()
        fun showListView()

    }


    interface PublishToDetailsActivity : PublishToView {

        fun setMainView(imgPath: String, mainTitle: String, originalTitleAndReleaseDate: String,
                        rating: Float, votesString: String, genres: String)
        fun setDescriptionView(description: String )
        fun setProductionView(companies: String, countries: String, budget: String, revenue: String)
        fun initRatingPresenterAndRateMovie(rate:Float, movieId: Int)
        fun hideIncludeViews()
        fun showIncludeVies()
        fun showRatingDialog()

    }

    interface PublishToMainActivity{

        fun startSearchActivity()
        fun startRatedMoviesActivity()
    }


    interface FragmentsPresenter {

        fun downloadMoviesPage()
        fun onItemClick(movieId: Int)

    }

    interface DetailsPresenter {

        fun downloadMovieData(movieId: Int)
        fun onRatingMenuTap()
        fun onRatingDialogSubmitButtonTap(rate: Float, movieId: Int)

    }

    interface MainActivityPresenter{

        fun onSearchMenuButtonClick()
        fun onRatedMoviesMenuButtonClick()

    }

    interface RatingPresenter{

        fun rateMovie(rate: Float, movieId: Int)
    }

}