package com.marcinbaranski.mymovies.ui

import android.content.Intent
import android.support.v4.app.Fragment
import android.view.View
import android.widget.LinearLayout
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import com.marcinbaranski.mymovies.Model.MovieResponse.MovieGeneral
import com.marcinbaranski.mymovies.Model.MoviesAdapter
import com.marcinbaranski.mymovies.Model.MyStrings
import com.marcinbaranski.mymovies.R
import com.marcinbaranski.mymovies.ui.DetailsActivity.DetailsActivity
import com.marcinbaranski.mymovies.ui.FooterView.FooterView

abstract class MyFragment : Fragment(), PresenterContracts.PublishToFragmentView, FooterView.OnFooterViewClick {

    lateinit var presenter : PresenterContracts.FragmentsPresenter
    lateinit var footerView : FooterView
    var listView : ListView? = null
    var adapter : MoviesAdapter? = null
    var mainView : View? = null

    open protected fun initListView(view: View?){
        listView = view?.findViewById<ListView>(R.id.list_view)
        listView?.setOnItemClickListener { _, _, position, _ ->
            presenter.onItemClick(adapter?.getItemId(position)!!.toInt())
        }
        initFooterView()
    }

    protected fun initFooterView(){
        footerView = FooterView(this.context, this)
        listView?.addFooterView(footerView.getFooterView())
    }

    override fun onFooterViewButtonClick() {
        presenter.downloadMoviesPage()
    }

    override fun onFooterViewInfoViewClick() {
        showToast(MyStrings.MSG_REFRESHING)
        presenter.downloadMoviesPage()
    }

    override fun showOnlyFooterButtonView(){
        footerView.showOnlyButton()
    }

    override fun showOnlyInfoFooterView(mainMessage: String, secondMessage: String){
        footerView.setInfoView(mainMessage, secondMessage)
        footerView.showOnlyInfoView()
    }

    override fun hideAllFooters(){
        footerView.hideAll()
    }

    override fun hideListView() {
        listView?.visibility = View.GONE
    }

    override fun showListView() {
        listView?.visibility = View.VISIBLE
    }

    override fun startDetailsActivity(movieId: Int) {
        val intent = Intent(this.context, DetailsActivity::class.java)
        intent.putExtra(MyStrings.INTENT_MOVIE_ID_KEY, movieId)
        startActivity(intent)
    }

    override fun showToast(message: String) {
        Toast.makeText(this.context, message, Toast.LENGTH_SHORT).show()
    }

    override fun addElementsToViewList(list: ArrayList<MovieGeneral>) {
        listView?.let{ lv ->
            if(lv.adapter == null) {
                this.adapter = MoviesAdapter(this.context, R.layout.movie_item, list)
                lv.adapter = adapter
                footerView.showOnlyButton()
            }else{
                adapter?.addAll(list)
                lv.deferNotifyDataSetChanged()
                footerView.showOnlyButton()
            }
        }
    }

    override fun showInformationView(mainMessage: String, secondMessage: String){
        createInfoView(mainMessage, secondMessage)?.visibility = View.VISIBLE
    }

    override fun hideInformationView() {
        (view?.findViewById<LinearLayout>(R.id.info_view))?.visibility = View.GONE
    }

    private fun createInfoView(mainMessage: String, secondMessage: String): View?{
        val info_view = mainView?.findViewById<View>(R.id.info_view)
        info_view?.let{
            info_view.setOnClickListener {
                hideInformationView()
                presenter.downloadMoviesPage()
            }
            (info_view.findViewById<TextView>(R.id.main_information_textview)).text = mainMessage
            (info_view.findViewById<TextView>(R.id.second_information_textview)).text = secondMessage
        }
        return info_view
    }
}