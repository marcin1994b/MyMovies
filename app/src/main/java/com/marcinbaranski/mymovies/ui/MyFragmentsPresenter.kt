package com.marcinbaranski.mymovies.ui

import com.marcinbaranski.mymovies.Model.MovieResponse.MovieResponse
import com.marcinbaranski.mymovies.Model.MyStrings
import com.marcinbaranski.mymovies.Model.Providers.NetworkProvider
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

abstract class MyFragmentsPresenter(val view : PresenterContracts.PublishToFragmentView,
                                    private val networkProvider: NetworkProvider)
    : PresenterContracts.FragmentsPresenter {

    var downloadedPages: Int = 0
    var totalPages: Int = -1

    override fun downloadMoviesPage(){
        when(networkProvider.isNetworkAvailable()){
            true -> {
                downloadedPages++
                view.showToast(MyStrings.MSG_DOWNLOADING)
                downloadData()
            }
            false -> {
                if(downloadedPages == 0){
                    view.hideListView()
                    view.showInformationView(MyStrings.ERR_NO_INTERNET_CONNECTION, MyStrings.TAP_TO_REFRESH)
                }else{
                    view.showOnlyInfoFooterView(MyStrings.ERR_NO_INTERNET_CONNECTION, MyStrings.TAP_TO_REFRESH)
                }
            }
        }
    }

    fun getDataFromObservable(single: Single<MovieResponse>){
        single.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleResponse, this::handleError)
    }

    private fun handleResponse(response: MovieResponse){
        totalPages = response.total_pages
        view.addElementsToViewList(response.results!!)
        view.showListView()
        if(downloadedPages < totalPages){
            view.showOnlyFooterButtonView()
        }else{
            view.hideAllFooters()
        }
    }

    private fun handleError(error: Throwable){
        view.showOnlyInfoFooterView(MyStrings.ERR_NO_INTERNET_CONNECTION, MyStrings.TAP_TO_REFRESH)
        downloadedPages--
    }

    override fun onItemClick(movieId: Int) {
        view.startDetailsActivity(movieId)
    }

    abstract fun downloadData()

}