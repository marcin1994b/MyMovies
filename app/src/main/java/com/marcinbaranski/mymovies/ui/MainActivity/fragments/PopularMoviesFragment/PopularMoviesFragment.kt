package com.marcinbaranski.mymovies.ui.MainActivity.fragments.PopularMoviesFragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.marcinbaranski.mymovies.Model.Providers.NetworkProvider
import com.marcinbaranski.mymovies.R
import com.marcinbaranski.mymovies.ui.MyFragment

class PopularMoviesFragment : MyFragment() {

    companion object {
        fun newInstance(): Fragment = PopularMoviesFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        mainView = inflater?.inflate(R.layout.fragment_layout, container, false)
        initListView(mainView)
        initPresenter()
        presenter.downloadMoviesPage()
        return mainView
    }

    private fun initPresenter() {
        presenter = PopularFragmentPresenter(this, NetworkProvider(context))
    }

}