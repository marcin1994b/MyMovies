package com.marcinbaranski.mymovies.ui.MainActivity.fragments.PopularMoviesFragment

import com.marcinbaranski.mymovies.Model.APIHandler
import com.marcinbaranski.mymovies.Model.Providers.NetworkProvider
import com.marcinbaranski.mymovies.ui.MyFragmentsPresenter
import com.marcinbaranski.mymovies.ui.PresenterContracts


class PopularFragmentPresenter(view: PresenterContracts.PublishToFragmentView,
                               networkProvider: NetworkProvider) : MyFragmentsPresenter(view, networkProvider) {

    override fun downloadData() {
        val apiHandler = APIHandler()
        getDataFromObservable(apiHandler.getPopularMovies(this.downloadedPages))
    }
}