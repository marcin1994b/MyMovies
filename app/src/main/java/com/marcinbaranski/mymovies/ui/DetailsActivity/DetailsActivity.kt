package com.marcinbaranski.mymovies.ui.DetailsActivity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.marcinbaranski.mymovies.Model.MyStrings
import com.marcinbaranski.mymovies.Model.Providers.NetworkProvider
import com.marcinbaranski.mymovies.Model.Providers.SharedPreferencesProvider
import com.marcinbaranski.mymovies.R
import com.marcinbaranski.mymovies.ui.PresenterContracts
import com.marcinbaranski.mymovies.ui.RatingDialog.RatingDialog
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_details.*
import kotlinx.android.synthetic.main.activity_details.view.*
import kotlinx.android.synthetic.main.description_details_layout.view.*
import kotlinx.android.synthetic.main.main_details_layout.view.*
import kotlinx.android.synthetic.main.production_details_layout.view.*

class DetailsActivity : AppCompatActivity(), PresenterContracts.PublishToDetailsActivity,
        RatingDialog.OnRatingDialogSubmitButtonClick {

    var presenter : PresenterContracts.DetailsPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        initToolbar()
        initPresenter()
        downloadMovieData()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.details_activity_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.rate -> {
                presenter?.onRatingMenuTap()
            }
        }
        return false
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }


    override fun showRatingDialog(){
        RatingDialog(this, this).getDialog()
    }

    override fun onRatingDialogSubmitButtonClick(rate: Float) {
        presenter?.onRatingDialogSubmitButtonTap(rate, getMovieId())
    }

    override fun initRatingPresenterAndRateMovie(rate:Float, movieId: Int){
        val ratePresenter: PresenterContracts.RatingPresenter = RatingPresenter(
                NetworkProvider(applicationContext),
                SharedPreferencesProvider(applicationContext),
                this)
        ratePresenter.rateMovie(rate, movieId)
    }

    override fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun showInformationView(mainMessage: String, secondMessage: String) {
        createInfoView(mainMessage, secondMessage).visibility = View.VISIBLE
    }

    override fun hideInformationView() {
        info_view.visibility = View.GONE
    }

    override fun hideIncludeViews(){
        deatils_view.visibility = View.GONE
    }

    override fun showIncludeVies() {
        deatils_view.visibility = View.VISIBLE
    }

    override fun setMainView(imgPath: String, mainTitle: String, originalTitleAndReleaseDate: String,
                             rating: Float, votesString: String, genres: String) {
        val view = findViewById(R.id.main_details_include) as View
        setImg(view, imgPath)
        setTopTitleTextView(view, mainTitle)
        setSecondTitleTextView(view, originalTitleAndReleaseDate)
        setRatingBarView(view, rating)
        setCountVoteTextView(view, votesString)
        setGenresTextView(view, genres)
    }

    override fun setDescriptionView(description: String) {
        setDescriptionTextView(description_details_include, description)
    }

    override fun setProductionView(companies: String, countries: String, budget: String, revenue: String) {
        val view = findViewById(R.id.production_details_include) as View
        setProductionCompaniesTextView(view, companies)
        setProductionCountriesTextView(view, countries)
        setBudgetTextView(view, budget)
        setRevenueTextView(view, revenue)

    }

    private fun createInfoView(mainMessage: String, secondMessage: String): View{
        info_view.setOnClickListener {
            hideInformationView()
            presenter?.downloadMovieData(getMovieId())
        }
        info_view.main_information_textview.text = mainMessage
        info_view.second_information_textview.text = secondMessage
        return info_view
    }

    private fun getMovieId(): Int = this.intent.getIntExtra(MyStrings.INTENT_MOVIE_ID_KEY, -1)

    private fun downloadMovieData(){
        presenter?.downloadMovieData(getMovieId())
    }

    // inits methods
    private fun initToolbar(){
        my_toolbar.title = getString(R.string.title_details_activity)
        setSupportActionBar(my_toolbar)
        supportActionBar?.let{
            it.setDisplayHomeAsUpEnabled(true)
            it.setDisplayShowHomeEnabled(true)
        }

    }
    private fun initPresenter(){
        presenter = DetailsPresenter(this, NetworkProvider(applicationContext))
    }

    // set text views methods
    private fun setTopTitleTextView(v: View, str: String){
        v.top_title_textview.text = str
    }
    private fun setSecondTitleTextView(v: View, str: String){
        v.second_title_and_release_date_textview.text = str
    }
    private fun setCountVoteTextView(v: View, str: String){
        v.count_vote_textview.text = str
    }
    private fun setGenresTextView(v: View, str: String){
        v.generes_textview.text = str
    }
    private fun setDescriptionTextView(v: View, str: String){
        v.description_textview.text = str
    }
    private fun setProductionCompaniesTextView(v: View, str: String){
        v.production_companies_textview.text = str
    }
    private fun setProductionCountriesTextView(v: View, str: String){
        v.production_countries_textview.text = str
    }
    private fun setBudgetTextView(v: View, str: String){
        v.budget_textview.text = str
    }
    private fun setRevenueTextView(v: View, str: String){
        v.revenue_textview.text = str
    }

    // set ratingbar view method
    private fun setRatingBarView(v: View, rating: Float){
        v.rating_bar.rating = rating
    }

    // set img view method
    private fun setImg(v: View, imgPath: String) {
        if (!imgPath.isEmpty()){
            Picasso.with(this)
                    .load(imgPath)
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.noimage)
                    .fit()
                    .into(v.image)
        }else{
            Picasso.with(this).load(R.drawable.noimage)
                    .fit()
                    .into(v.image)
        }
    }
}