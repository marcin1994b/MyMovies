package com.marcinbaranski.mymovies.ui.FooterView

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import com.marcinbaranski.mymovies.R
import kotlinx.android.synthetic.main.listview_footer_view.view.*

class FooterView(context: Context, view: OnFooterViewClick){

    private val mainView : View

    interface OnFooterViewClick{

        fun onFooterViewButtonClick()

        fun onFooterViewInfoViewClick()
    }

    init {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        mainView = inflater.inflate(R.layout.listview_footer_view, null)
        initButton(view)
        initInfoView(view)
    }

    fun getFooterView() : View = mainView

    fun setInfoView(firstText: String, secondText: String){
        setMainTextView(firstText)
        setSecondTextView(secondText)
    }

    fun showOnlyButton(){
        hideInfoView()
        showButton()
    }

    fun showOnlyInfoView(){
        hideButton()
        showInfoView()
    }

    fun hideAll() {
        hideButton()
        hideInfoView()
    }

    // PRIVATE METHODS

    private fun initButton(v: OnFooterViewClick){
        mainView.load_more_button.setOnClickListener{ v.onFooterViewButtonClick() }
    }

    private fun initInfoView(v: OnFooterViewClick){
        mainView.info_view.setOnClickListener{ v.onFooterViewInfoViewClick() }
    }

    private fun setMainTextView(str: String){
        mainView.info_view.main_information_textview.text = str
    }

    private fun setSecondTextView(str: String){
        mainView.info_view.second_information_textview.text = str
    }

    private fun hideButton(){
        mainView.load_more_button.visibility = View.GONE
    }

    private fun showButton(){
        mainView.load_more_button.visibility = View.VISIBLE
    }

    private fun hideInfoView(){
        mainView.info_view.visibility = View.GONE
    }

    private fun showInfoView(){
        mainView.info_view.visibility = View.VISIBLE
    }

}