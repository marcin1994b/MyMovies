package com.marcinbaranski.mymovies.ui.MainActivity

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.marcinbaranski.mymovies.Model.MyStrings
import com.marcinbaranski.mymovies.R
import com.marcinbaranski.mymovies.ui.MainActivity.fragments.PopularMoviesFragment.PopularMoviesFragment
import com.marcinbaranski.mymovies.ui.MainActivity.fragments.TopRatedMoviesFragment.TopRatedMoviesFragment
import com.marcinbaranski.mymovies.ui.MainActivity.fragments.UpcomingMoviesFragment.UpcomingMoviesFragment
import com.marcinbaranski.mymovies.ui.MyRatedMoviesActivity.RatedMoviesActivity
import com.marcinbaranski.mymovies.ui.PresenterContracts
import com.marcinbaranski.mymovies.ui.SearchActivity.SearchActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), PresenterContracts.PublishToMainActivity {

    lateinit var presenter: PresenterContracts.MainActivityPresenter

    class MyPagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {

        override fun getCount(): Int = NUM_ITEMS

        override fun getItem(position: Int): Fragment? = when (position) {
            0 -> TopRatedMoviesFragment.newInstance()
            1 -> PopularMoviesFragment.newInstance()
            2 -> UpcomingMoviesFragment.newInstance()
            else -> null
        }

        override fun getPageTitle(position: Int): CharSequence = when(position){
            0 -> MyStrings.TITLE_TOP_MOVIES_FRAGMENT
            1 -> MyStrings.TITLE_MOST_POPULAR_MOVIES_FRAGMENT
            2 -> MyStrings.TITLE_UPCOMING_MOVIES_FRAGMENT
            else -> ""
        }

        companion object {
            private val NUM_ITEMS = 3
        }
    }

    var adapterViewPager: FragmentPagerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initToolbar()
        initPresenter()
        initViewPager()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_activity_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.search -> {
                presenter.onSearchMenuButtonClick()
            }
            R.id.rate -> {
                presenter.onRatedMoviesMenuButtonClick()
            }
        }
        return false
    }


    override fun startSearchActivity() {
        val intent = Intent(this, SearchActivity::class.java)
        startActivity(intent)
    }

    override fun startRatedMoviesActivity() {
        val intent = Intent(this, RatedMoviesActivity::class.java)
        startActivity(intent)
    }

    private fun initPresenter(){
        presenter = MainActivityPresenter(this)
    }

    private fun initViewPager(){
        adapterViewPager = MyPagerAdapter(supportFragmentManager)
        vpPager.adapter = adapterViewPager
    }

    private fun initToolbar(){
        my_toolbar.title = getString(R.string.title_main_activity)
        setSupportActionBar(my_toolbar)

    }
}
