package com.marcinbaranski.mymovies.ui.DetailsActivity

import com.marcinbaranski.mymovies.Model.APIHandler
import com.marcinbaranski.mymovies.Model.MovieResponse.MovieGeneral
import com.marcinbaranski.mymovies.Model.MyStrings
import com.marcinbaranski.mymovies.Model.Providers.DetailsViewDataProvider
import com.marcinbaranski.mymovies.Model.Providers.NetworkProvider
import com.marcinbaranski.mymovies.ui.PresenterContracts
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class DetailsPresenter(private val view: PresenterContracts.PublishToDetailsActivity,
                       private val networkProvider: NetworkProvider) :
        PresenterContracts.DetailsPresenter {

    override fun downloadMovieData(movieId: Int){
        when(networkProvider.isNetworkAvailable()){
            true -> {
                view.showToast(MyStrings.MSG_DOWNLOADING)
                downloadData(movieId)
            }
            false -> {
                view.hideIncludeViews()
                view.showInformationView(MyStrings.ERR_NO_INTERNET_CONNECTION, MyStrings.TAP_TO_REFRESH)
            }
        }
    }

    override fun onRatingMenuTap(){
        view.showRatingDialog()
    }

    override fun onRatingDialogSubmitButtonTap(rate: Float, movieId: Int){
        view.initRatingPresenterAndRateMovie(rate, movieId)
    }

    private fun downloadData(movieId: Int) {
        val apiHandler = APIHandler()
        getDataFromObservable(apiHandler.getMovieDetails(movieId))
    }

    private fun getDataFromObservable(single: Single<MovieGeneral>){
        single.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleResponse, this::handleError)
    }

    private fun handleResponse(response: MovieGeneral){
        setView((response))
        view.showIncludeVies()
        view.hideInformationView()
        view.showToast(MyStrings.MSG_DATA_DOWNLOADED_SUCCESSFUL)
    }

    private fun handleError(error: Throwable){
        view.showToast(MyStrings.ERR_UPS_WE_GOT_SOME_PROBLEM)
    }

    private fun setView(movieData: MovieGeneral){
        val movieDataProvider = DetailsViewDataProvider(movieData)
        view.setMainView(
                movieDataProvider.getImgPathFromMovieData(),
                movieDataProvider.getTitleFromMovieData(),
                movieDataProvider.getOriginalTitleAndReleaseDateFromMovieData(),
                movieDataProvider.getRatingFromMovieData(),
                "",
                movieDataProvider.getGenresFromMovieData())
        view.setDescriptionView(movieDataProvider.getDescriptionFromMovieData())
        view.setProductionView(
                movieDataProvider.getCompanyNamesFromMovieData(),
                movieDataProvider.getCountriesNameFromMovieData(),
                movieDataProvider.getBudgetFromMovieData(),
                movieDataProvider.getRevenueFromMovieData())
    }
}