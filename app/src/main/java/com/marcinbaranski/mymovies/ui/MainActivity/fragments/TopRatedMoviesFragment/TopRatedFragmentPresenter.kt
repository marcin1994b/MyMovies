package com.marcinbaranski.mymovies.ui.MainActivity.fragments.TopRatedMoviesFragment

import com.marcinbaranski.mymovies.Model.APIHandler
import com.marcinbaranski.mymovies.Model.Providers.NetworkProvider
import com.marcinbaranski.mymovies.ui.MyFragmentsPresenter
import com.marcinbaranski.mymovies.ui.PresenterContracts

class TopRatedFragmentPresenter(view : PresenterContracts.PublishToFragmentView,
                                networkProvider: NetworkProvider) : MyFragmentsPresenter(view, networkProvider) {

    override fun downloadData(){
        val apiHandler = APIHandler()
        getDataFromObservable(apiHandler.getTopRatedMovies(this.downloadedPages))
    }
}