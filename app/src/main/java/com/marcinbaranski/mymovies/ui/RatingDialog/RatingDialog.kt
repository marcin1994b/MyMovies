package com.marcinbaranski.mymovies.ui.RatingDialog

import android.app.Dialog
import android.content.Context
import android.widget.RatingBar
import com.marcinbaranski.mymovies.Model.MyStrings
import com.marcinbaranski.mymovies.R
import kotlinx.android.synthetic.main.rank_dialog.*

class RatingDialog(val context: Context, val view: OnRatingDialogSubmitButtonClick) {

    interface OnRatingDialogSubmitButtonClick{

        fun onRatingDialogSubmitButtonClick(rate: Float)

    }

    fun getDialog(){
        val rankDialog = Dialog(context, R.style.FullHeightDialog)
        rankDialog.setContentView(R.layout.rank_dialog)
        rankDialog.setCancelable(true)
        val ratingBar = initRatingBar(rankDialog)
        initTextView(rankDialog)
        initButton(rankDialog, ratingBar)
        rankDialog.show()
    }

    private fun initRatingBar(v: Dialog) : RatingBar{
        val ratingBar = v.findViewById<RatingBar>(R.id.dialog_ratingbar)
        ratingBar.rating = 0.0F
        return ratingBar
    }
    
    private fun initTextView(v: Dialog){
        v.rank_dialog_text1.text = MyStrings.RATE_THE_MOVIE
    }

    private fun initButton(v: Dialog, ratingBar: RatingBar){
        v.rank_dialog_button.setOnClickListener({
            view.onRatingDialogSubmitButtonClick(ratingBar.rating)
            v.dismiss()
        })
    }
}
