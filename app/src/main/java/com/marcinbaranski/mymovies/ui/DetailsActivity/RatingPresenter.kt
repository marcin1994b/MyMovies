package com.marcinbaranski.mymovies.ui.DetailsActivity

import com.marcinbaranski.mymovies.Model.APIHandler
import com.marcinbaranski.mymovies.Model.MovieResponse.RateQueryBody
import com.marcinbaranski.mymovies.Model.MovieResponse.RatingResponse
import com.marcinbaranski.mymovies.Model.MyStrings
import com.marcinbaranski.mymovies.Model.Providers.AuthenticationProvider
import com.marcinbaranski.mymovies.Model.Providers.NetworkProvider
import com.marcinbaranski.mymovies.Model.Providers.SharedPreferencesProvider
import com.marcinbaranski.mymovies.ui.PresenterContracts
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class RatingPresenter(private val networkProvider: NetworkProvider,
                      private val sharedPreferencesProvider: SharedPreferencesProvider,
                      private val view: PresenterContracts.PublishToView)
    : PresenterContracts.RatingPresenter, AuthenticationProvider.AuthenticationProvider {

    private var rating = 0.0F
    private var movieId = 0

    override fun rateMovie(rate: Float, movieId: Int) {
        setRatingAndMovieId(rate, movieId)
        if(networkProvider.isNetworkAvailable()) {
            val guestId = getGuestSessionId()
            when(guestId.isEmpty()){
                true -> getNewAuthenticateGuestId()
                false -> postRateToAPI(guestId, this.rating, this.movieId)
            }
        }else{
            view.showToast(MyStrings.ERR_NO_INTERNET_CONNECTION)
        }
    }

    override fun onAuthIdReturn(isSuccessful: Boolean, guestSessionId: String, message: String) {
        when(isSuccessful){
            true -> {
                sharedPreferencesProvider.saveGuestIdToSharedPreferences(guestSessionId)
                postRateToAPI(getGuestSessionId(), this.rating, this.movieId)
            }
            false -> view.showToast(message)
        }
    }

    private fun setRatingAndMovieId(rate: Float, movieId: Int){
        this.rating = rate*2
        this.movieId = movieId
    }

    private fun postRateToAPI(guestId: String, rate: Float, movieId: Int){
        val apiHandler = APIHandler()
        getDataFromObservable(apiHandler.postMovieRate(guestId, RateQueryBody(rate), movieId))
    }

    private fun getDataFromObservable(single: Single<RatingResponse>){
        single.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleAuthResponse, this::handleAuthError)
    }

    private fun getGuestSessionId(): String =
            sharedPreferencesProvider.getGuestIdFromSharedPreferences()

    private fun getNewAuthenticateGuestId(){
        val authProvider = AuthenticationProvider(this)
        authProvider.getAuthenticateGuestId()
    }

    private fun handleAuthResponse(response: RatingResponse){
        view.showToast(response.status_message.toString())
    }

    private fun handleAuthError(error: Throwable){
        view.showToast(MyStrings.ERR_UPS_WE_GOT_SOME_PROBLEM)
    }

}