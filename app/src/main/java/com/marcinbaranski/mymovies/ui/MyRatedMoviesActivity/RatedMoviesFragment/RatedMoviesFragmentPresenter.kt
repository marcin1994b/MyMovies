package com.marcinbaranski.mymovies.ui.MyRatedMoviesActivity.RatedMoviesFragment

import com.marcinbaranski.mymovies.Model.APIHandler
import com.marcinbaranski.mymovies.Model.Providers.NetworkProvider
import com.marcinbaranski.mymovies.ui.MyFragmentsPresenter
import com.marcinbaranski.mymovies.ui.PresenterContracts

class RatedMoviesFragmentPresenter(view: PresenterContracts.PublishToFragmentView,
                                   private val guestID: String,
                                   networkProvider: NetworkProvider) :
        PresenterContracts.FragmentsPresenter, MyFragmentsPresenter(view, networkProvider){

    override fun downloadData() {
        val apiHandler = APIHandler()
        getDataFromObservable(apiHandler.getGuestSessionRatedMovies(guestID, this.downloadedPages))
    }
}