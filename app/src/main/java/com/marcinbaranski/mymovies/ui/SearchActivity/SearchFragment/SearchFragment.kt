package com.marcinbaranski.mymovies.ui.SearchActivity.SearchFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.marcinbaranski.mymovies.Model.Providers.NetworkProvider
import com.marcinbaranski.mymovies.R
import com.marcinbaranski.mymovies.ui.MyFragment
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit

class SearchFragment : MyFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        mainView = inflater?.inflate(R.layout.fragment_layout, container, false)
        initListView(mainView)
        return mainView
    }


    fun setObservableOnEditText(editTextObservable: Observable<CharSequence>){
        var lastStr = ""
        editTextObservable.debounce(500, TimeUnit.MILLISECONDS)
                .map { it.toString().trim() }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    if(it.isNotBlank()) {
                        if (lastStr != it) {
                            lastStr = it
                            downloadData(it)
                        }
                    }else{
                        hideAllFooters()
                        clearListView()
                    }
                }
    }

    fun downloadData(query: String){
        if(!query.isEmpty()) {
            initPresenter(query)
            presenter.downloadMoviesPage()
            clearListView()
            footerView.hideAll()
        }else{
            clearListView()
            footerView.hideAll()
        }
    }

    private fun initPresenter(query: String){
        presenter = SearchFragmentPresenter(this, query, NetworkProvider(context))
    }

    private fun clearListView(){
        adapter?.clear()
        listView?.deferNotifyDataSetChanged()
    }

}