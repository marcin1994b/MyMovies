package com.marcinbaranski.mymovies.Model

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.marcinbaranski.mymovies.Model.MovieResponse.*
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

class APIHandler {

    private interface APIInterface {

        @GET("movie/top_rated")
        fun getTopRatedMovies(@Query("api_key") apiKey: String,
                              @Query("page") page: Int) : Single<MovieResponse>

        @GET("movie/popular")
        fun getPopularMovies(@Query("api_key") apiKey: String,
                             @Query("page") page: Int) : Single<MovieResponse>

        @GET("movie/upcoming")
        fun getUpcomingMovies(@Query("api_key") apiKey: String,
                              @Query("page") page: Int) : Single<MovieResponse>

        @GET("movie/{id}")
        fun getMovieDetails(@Path("id") id: Int, @Query("api_key") apiKey: String) : Single<MovieGeneral>

        @GET("authentication/guest_session/new")
        fun getGuestSessionNewId(@Query("api_key") apiKey: String) : Single<AuthenticationResponse>

        @GET("guest_session/{guest_session_id}/rated/movies")
        fun getGuestSessionRatedMovies(@Path("guest_session_id") guestSessionId: String,
                                       @Query("api_key") apiKey: String,
                                       @Query("page") page: Int,
                                       @Query("sort_by") sortBy: String) : Single<MovieResponse>

        @GET("search/movie")
        fun getSearchMovies(@Query("api_key") apiKey: String,
                            @Query("query") query:String,
                            @Query("page") page: Int) : Single<MovieResponse>

        @Headers("Content-Type: application/json;charset=utf-8")
        @POST("movie/{movie_id}/rating")
        fun postMovieRate(@Path("movie_id") movieId: Int,
                          @Query("api_key") apiKey: String,
                          @Query("guest_session_id") guestSessionId: String,
                          @Body rateQueryBody: RateQueryBody) : Single<RatingResponse>

    }

    private object APIClient {

        val BASIC_URL : String = "http://api.themoviedb.org/3/"
        var retrofit : Retrofit

        init {
            retrofit = Retrofit.Builder()
                    .baseUrl(BASIC_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()
        }
    }

    private val API_KEY : String = ""
    private val apiService : APIInterface = APIClient.retrofit.create(APIInterface::class.java)

    fun getTopRatedMovies(page : Int) : Single<MovieResponse> =
            apiService.getTopRatedMovies(API_KEY, page)

    fun getPopularMovies(page : Int) : Single<MovieResponse> =
            apiService.getPopularMovies(API_KEY, page)

    fun getUpcomingMovies(page : Int) : Single<MovieResponse> =
            apiService.getUpcomingMovies(API_KEY, page)

    fun getMovieDetails(id: Int) : Single<MovieGeneral> = apiService.getMovieDetails(id, API_KEY)

    fun getGuestSessionNewId() : Single<AuthenticationResponse> =
            apiService.getGuestSessionNewId(API_KEY)

    fun getGuestSessionRatedMovies(guestSessionId: String, page: Int) : Single<MovieResponse> =
            apiService.getGuestSessionRatedMovies(guestSessionId, API_KEY, page, "created_at.desc")

    fun getSearchMovies(query: String, page: Int) : Single<MovieResponse> =
            apiService.getSearchMovies(API_KEY, query, page)

    fun postMovieRate(guestSessionId: String, rateQueryBody: RateQueryBody, movieId: Int):
            Single<RatingResponse> = apiService.postMovieRate(movieId, API_KEY,
            guestSessionId, rateQueryBody)

}