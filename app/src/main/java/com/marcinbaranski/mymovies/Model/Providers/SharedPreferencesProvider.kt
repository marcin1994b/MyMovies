package com.marcinbaranski.mymovies.Model.Providers

import android.app.Activity
import android.content.Context
import com.marcinbaranski.mymovies.Model.MyStrings

class SharedPreferencesProvider(private val context: Context) {

    fun saveGuestIdToSharedPreferences(id: String){
        val preferences = context.getSharedPreferences(MyStrings.PREFERENCES_NAME, Activity.MODE_PRIVATE)
        preferences?.let{
            val preferencesEditor = preferences.edit()
            preferencesEditor.putString(MyStrings.PREFERENCES_GUEST_ID, id)
            preferencesEditor.commit()
        }
    }

    fun getGuestIdFromSharedPreferences() : String{
        val preferences = context.getSharedPreferences(MyStrings.PREFERENCES_NAME, Activity.MODE_PRIVATE)
        preferences?.let {
            return preferences.getString(MyStrings.PREFERENCES_GUEST_ID, "")
        }
        return ""
    }
}