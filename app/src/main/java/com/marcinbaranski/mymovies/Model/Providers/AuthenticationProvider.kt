package com.marcinbaranski.mymovies.Model.Providers

import com.marcinbaranski.mymovies.Model.APIHandler
import com.marcinbaranski.mymovies.Model.MovieResponse.AuthenticationResponse
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class AuthenticationProvider(private val presenter: AuthenticationProvider) {

    interface AuthenticationProvider{
        fun onAuthIdReturn(isSuccessful: Boolean, guestSessionId: String, message: String)
    }

    fun getAuthenticateGuestId(){
        val apiHandler = APIHandler()
        getDataFromObservable(apiHandler.getGuestSessionNewId())
    }

    private fun getDataFromObservable(single: Single<AuthenticationResponse>){
        single.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleAuthResponse, this::handleAuthError)
    }

    private fun handleAuthResponse(response: AuthenticationResponse){
        presenter.onAuthIdReturn(true, response.guest_session_id, "")
    }

    private fun handleAuthError(error: Throwable){
        presenter.onAuthIdReturn(false, "", error.message!!)
    }

}