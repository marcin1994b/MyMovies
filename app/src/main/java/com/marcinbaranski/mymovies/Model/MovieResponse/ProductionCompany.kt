package com.marcinbaranski.mymovies.Model.MovieResponse

data class ProductionCompany(val id: Int,
                             val name: String)