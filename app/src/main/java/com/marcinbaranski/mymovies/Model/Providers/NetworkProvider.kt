package com.marcinbaranski.mymovies.Model.Providers

import android.content.Context
import android.net.ConnectivityManager

class NetworkProvider(private val context: Context) {

    fun isNetworkAvailable(): Boolean {
        val connectivityManager = this.context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }
}