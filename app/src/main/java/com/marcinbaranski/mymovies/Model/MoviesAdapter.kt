package com.marcinbaranski.mymovies.Model

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import com.marcinbaranski.mymovies.Model.MovieResponse.MovieGeneral
import com.marcinbaranski.mymovies.R
import com.squareup.picasso.Picasso

class MoviesAdapter(context: Context, resource: Int, var list: ArrayList<MovieGeneral>) :
        ArrayAdapter<MovieGeneral>(context, resource, list) {

    val BASIC_IMG_URL = "http://image.tmdb.org/t/p/w185//"

    override fun getItemId(position: Int): Long {
        return list[position].id.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        var view = convertView
        if(view == null){
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.movie_item, null)
        }

        view?.let{
            val item = list[position]
            setTitle(item.title, it)
            setOriginalTitle(item.original_title, it)
            setRatingBar(item.vote_average, it)
            setRatingView(item.rating, it)
            setReleaseDateView(item.release_date, it)
            setPosterView(item.poster_path, it)
        }

        return view
    }

    private fun setTitle(title: String?, v: View){
        title?.let{
            ((v.findViewById<TextView>(R.id.top_title))).text = title
        }
    }

    private fun setOriginalTitle(title: String?, v: View){
        title?.let {
            ((v.findViewById<TextView>(R.id.second_title))).text = title
        }
    }

    private fun setRatingBar(rating: Float?, v: View){
        rating?.let{
            ((v.findViewById<RatingBar>(R.id.rating_bar))).rating = (rating.div(2))
        }
    }

    private fun setRatingView(rating: Float?, v: View){
        rating?.let{
            val str = StringBuilder()
            str.append("( your vote: ").append(rating.div(2).toString()).append(" )")
            ((v.findViewById<TextView>(R.id.count_vote))).text = str
        }
    }

    private fun setReleaseDateView(releaseDate: String?, v: View){
        releaseDate?.let{
            val str = StringBuilder()
            str.append("(").append(releaseDate).append(")")
            ((v.findViewById<TextView>(R.id.release_date))).text = str
        }
    }

    private fun setPosterView(path: String?, v: View){
        path?.let{
            val str = StringBuilder()
            str.append(BASIC_IMG_URL).append(path)
            Picasso
                    .with(context)
                    .load(str.toString())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.noimage)
                    .fit()
                    .into(v.findViewById<ImageView>(R.id.image))
        }
    }

}