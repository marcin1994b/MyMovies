package com.marcinbaranski.mymovies.Model.MovieResponse

data class Genre(val id: Int,
                 val name: String)