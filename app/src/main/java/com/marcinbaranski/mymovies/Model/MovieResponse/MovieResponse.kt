package com.marcinbaranski.mymovies.Model.MovieResponse


data class MovieResponse(val page: Int,
                         val total_results: Int,
                         val total_pages: Int,
                         val results: ArrayList<MovieGeneral>?)