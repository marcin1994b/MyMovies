package com.marcinbaranski.mymovies.Model.MovieResponse

data class AuthenticationResponse(val success: String,
                                  val guest_session_id: String,
                                  val expires_at: String)
