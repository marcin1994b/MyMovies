package com.marcinbaranski.mymovies.Model.MovieResponse

data class RatingResponse(val status_code: Int?,
                          val status_message: String?)