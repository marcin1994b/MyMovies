package com.marcinbaranski.mymovies.Model.Providers

import com.marcinbaranski.mymovies.Model.MovieResponse.MovieGeneral
import com.marcinbaranski.mymovies.Model.MyStrings
import java.text.DecimalFormat


class DetailsViewDataProvider(private val movieData: MovieGeneral) {

    private val EMPTY_STRING = ""

    fun getImgPathFromMovieData(): String{
        movieData.poster_path?.let {
            val str = StringBuilder()
            str.append(MyStrings.BASIC_IMG_URL).append(movieData.poster_path)
            return str.toString()
        }
        return EMPTY_STRING
    }

    fun getTitleFromMovieData() : String{
        movieData.title?.let {
            return movieData.title
        }
        return EMPTY_STRING
    }

    fun getOriginalTitleAndReleaseDateFromMovieData(): String{
        movieData.original_title?.let{ original_title ->
            movieData.release_date?.let { release_date ->
                val str = StringBuilder()
                str.append(original_title)
                        .append(" (")
                        .append(release_date)
                        .append(")")
                return str.toString()
            }
        }
        return EMPTY_STRING
    }

    fun getRatingFromMovieData(): Float{
        movieData.vote_average?.let{
            return movieData.vote_average.toFloat()/2
        }
        return 0.0F
    }

    fun getGenresFromMovieData(): String{
        movieData.genres?.let{
            if(movieData.genres.size > 0) {
                val str = StringBuilder()
                for (item in movieData.genres) {
                    str.append(item.name).append(", ")
                }
                str.delete(str.length - 2, str.length - 1)
                return str.toString()
            }
        }
        return EMPTY_STRING
    }

    fun getDescriptionFromMovieData(): String{
        movieData.overview?.let {
            return movieData.overview
        }
        return EMPTY_STRING
    }

    fun getCompanyNamesFromMovieData(): String{
        movieData.production_companies?.let {
            if(movieData.production_companies.size > 0) {
                val str = StringBuilder()
                for (item in movieData.production_companies) {
                    str.append(item.name).append(", ")
                }
                str.delete(str.length - 2, str.length - 1)
                return str.toString()
            }
        }
        return EMPTY_STRING
    }

    fun getCountriesNameFromMovieData(): String{
        movieData.production_countries?.let {
            if(movieData.production_countries.size > 0) {
                val str = StringBuilder()
                for (item in movieData.production_countries) {
                    str.append(item.name).append(", ")
                }
                str.delete(str.length - 2, str.length - 1)
                return str.toString()
            }
        }
        return EMPTY_STRING
    }

    fun getBudgetFromMovieData(): String{
        movieData.budget?.let{
            return getCash(movieData.budget)
        }
        return EMPTY_STRING
    }

    fun getRevenueFromMovieData(): String{
        movieData.revenue?.let {
            return getCash(movieData.revenue)
        }
        return EMPTY_STRING
    }

    private fun getCash(cash: Int): String{
        if(cash > 0) {
            val formatter = DecimalFormat("#,###")
            val str = StringBuilder()
            str.append(formatter.format(cash.toDouble()))
            str.insert(0, '$')
            return str.toString()
        }
        return EMPTY_STRING
    }
}