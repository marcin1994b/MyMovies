package com.marcinbaranski.mymovies.Model

object MyStrings {

    val MSG_DATA_DOWNLOADED_SUCCESSFUL = "Downloaded successful."
    val MSG_DOWNLOADING = "Downloading..."
    val MSG_REFRESHING = "Refreshing..."

    val ERR_NO_INTERNET_CONNECTION = "Ups! No Internet connection..."
    val ERR_UPS_WE_GOT_SOME_PROBLEM = "Ups! We got some problem... :("

    val TAP_TO_REFRESH = "Tap to refresh"
    val RATE_THE_MOVIE = "Rate the movie"

    val INTENT_MOVIE_ID_KEY = "movie_id"

    val PREFERENCES_NAME : String = "com.mymovies.marcinbaranski"
    val PREFERENCES_GUEST_ID: String = "guest_id"

    val BASIC_IMG_URL = "http://image.tmdb.org/t/p/w185//"

    val TITLE_TOP_MOVIES_FRAGMENT = "TopRated"
    val TITLE_MOST_POPULAR_MOVIES_FRAGMENT = "MostPopular"
    val TITLE_UPCOMING_MOVIES_FRAGMENT = "Upcoming"

    val EMPTY_STRING = ""
}