package com.marcinbaranski.mymovies.Model.MovieResponse


data class RateQueryBody(val value: Float)